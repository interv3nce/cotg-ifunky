# COTG IFunky
COTG IFunky script is a fork of original Cfunky/Dfunky/MFunky scripts.
In order to install you should have Tampermonkey (for Chrome) or Greasemonkey (for Firefox). 
To work properly other versions of a script (CFunky, Dfunky, Mfunky) should be disabled.
